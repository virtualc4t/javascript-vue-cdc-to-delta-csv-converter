export const Strings = {
    en: {
        // footer
        data_collection: 'No data will be collected nor transmitted, it is processed entirely by your browser',
        affiliation: 'This application is not affiliated with <a href="https://crypto.com/" target="_blank" rel="noopener noreferrer">Crypto.com</a> and <a href="https://delta.app" target="_blank" rel="noopener noreferrer">Delta</a>',
        risk: 'Use at your own risk',
        source_codes: 'Source codes',
        hosted: '☁ Hosted on Usonyx ☁',
        // import
        cdc_instruction: 'Import Crypto.com exported CSV',
        drag_and_drop_possible: 'You can also drag and drop the CSV file here',
        invalid_file_type: 'Invalid file type',
        error_reading_file: 'Error reading file, try manually pasting CSV above',
        invalid_header: 'Invalid Crypto.com CSV header',
        invalid_csv: 'CSV failed to parse',
        // process
        process_instruction: 'Process converted data',
        summary: 'Summary',
        breakdown: 'Breakdown',
        currency: 'Currency',
        total: 'Total',
        timestamp: 'Timestamp (UTC)',
        type: 'Type',
        exchange: 'Exchange',
        base_amount: 'Base amount',
        base_currency: 'Base currency',
        quote_amount: 'Quote amount',
        quote_currency: 'Quote currency',
        sync: 'Sync',
        sent_received: 'Sent/Received',
        sent_to: 'Sent to',
        notes: 'Notes',
        // misc
        submit: 'Submit',
        retry: 'Retry',
        download: 'Download'
    },
    default: function () {
        if (document.documentElement.lang)
            return this[document.documentElement.lang];
        return this.en;
    }
};